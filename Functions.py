# #################################################### #
# File with all the functions used in this project
# Creation date: 11/2019
# By: Kerlann Le Calvez
# #################################################### #

# #################################################### #

# Import libraries and modules
# To import csv files into dataframes
import pandas as pd
# To use the SVM classifier and to preprocess the data
from sklearn import svm, preprocessing, model_selection
# To measure the performance of the model
from sklearn.metrics import confusion_matrix
# To calculate the end date of treatment
import datetime
# To sort the lists
from operator import itemgetter
# To import and export the csv files
import csv
# To change the path
import os


# #################################################### #


# ##################################################### #
# General functions to import and export into csv files #
# ##################################################### #


def import_csv_file(path, file):
    # Change the pathway
    os.chdir(path)

    # Import the file into a dictionary that is then converted to a list - headers are kept
    with open(file) as f:
        reader = csv.DictReader(f)
        result = [r for r in reader]

    return(result)


def export_into_csv(path, file, final_list):
    # Change the pathway
    os.chdir(path)

    # Export the list into the txt file.
    with open(file, "w") as f_csv:
        writer = csv.writer(f_csv, delimiter=',', lineterminator='\n')
        writer.writerows(final_list)


# #################################################### #


# ############################### #
# Functions to clean the raw data #
# ############################### #


def clean_doses(list_raw):
    # Create empty list to add the new results
    result_dose_clean = []

    # When the actual dose received is negative, replace it by the prescribed dose
    for row in list_raw:
        if float(row['RTACTUALDOSE']) <= 0:
            result_dose_clean.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['PRESCRIBEDDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY']))
        else:
            result_dose_clean.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY']))

    return(result_dose_clean)


def clean_intent(list_raw):
    # Create empty list to add the new results
    result_intent_clean = []

    # Change the 1 and 2 found as the intent. 1 is "Palliative" and 2, "Not palliative". There might be missing values
    for row in list_raw:
        try:
            if int(row[10]) == 1:
                result_intent_clean.append((row[0], row[1], float(row[2]), float(row[3]), row[4], row[5], row[6], row[7], float(row[8]), float(row[9]), 'Palliative', row[11]))
            elif int(row[10]) == 2:
                result_intent_clean.append((row[0], row[1], float(row[2]), float(row[3]), row[4], row[5], row[6], row[7], float(row[8]), float(row[9]), 'Not Palliative', row[11]))
        except ValueError:
            result_intent_clean.append((row[0], row[1], float(row[2]), float(row[3]), row[4], row[5], row[6], row[7], float(row[8]), float(row[9]), row[10], row[11]))

    return(result_intent_clean)


def clean_types(list_raw):
    result = []

    for row in list_raw:
        result.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), datetime.datetime.strptime(row['TREATMENTSTARTDATE'], "%d/%m/%Y %H:%M"), datetime.datetime.strptime(row['TREATMENTENDDATE'], "%d/%m/%Y %H:%M"), row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE']))

    result = sorted(result, key=itemgetter(0, 1, 4, 2, 3))

    return(result)


# #################################################### #


# ###################################### #
# Functions for the rule-based algorithm #
# ###################################### #


def RuleBased(list_raw):
    pallia = []

    for row in list_raw:

        ##

        # Prophylactic and 10#
        # => Not palliative
        if 'O' in row['RTTREATMENTREGION'] and (float(row['ACTUALFRACTIONS']) in [9, 10]):
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], 'Not Palliative'))

        ####

        # Without any precision of the cancer, just by looking at the dose and fraction:
        # - 6 in 1# or
        # - 8 in 1# or
        # - 9 in 1# or
        # - 10 in 1# or
        # - 21 in 3# or
        # - 16 in 4# or
        # - 20 in 5# or
        # - 30 in 5# or
        # - 30 in 10# or
        # - 36 in 12# or
        # - 39 in 13#
        # => Palliative
        elif (row['RADIOTHERAPYDIAGNOSISICD'] != 'C61' and row['RTTREATMENTANATOMICALSITE'][0:3] != 'Z15') and ((round(float(row['RTACTUALDOSE']), 0) in [6, 8, 9, 10] and float(row['ACTUALFRACTIONS']) == 1) or (round(float(row['RTACTUALDOSE']), 0) == 21 and float(row['ACTUALFRACTIONS']) == 3) or (round(float(row['RTACTUALDOSE']), 0) == 16 and float(row['ACTUALFRACTIONS']) == 4) or (round(float(row['RTACTUALDOSE']), 0) in [20, 30] and float(row['ACTUALFRACTIONS']) == 5) or (round(float(row['RTACTUALDOSE']), 0) == 30 and float(row['ACTUALFRACTIONS']) == 10) or (round(float(row['RTACTUALDOSE']), 0) == 36 and float(row['ACTUALFRACTIONS']) == 12) or (round(float(row['RTACTUALDOSE']), 0) == 39 and float(row['ACTUALFRACTIONS']) == 13)):
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], 'Palliative'))

        ####

        # Head & neck cancer and:
        # - 14 in 4#
        # - 40 in 10#
        # - 37 in 13#
        # - 38 in 13#
        # => Palliative
        elif row['RADIOTHERAPYDIAGNOSISICD'] in ['C00', 'C01', 'C02', 'C03', 'C04', 'C05', 'C06', 'C07', 'C08', 'C09', 'C10', 'C11', 'C12', 'C13', 'C14'] and ((round(float(row['RTACTUALDOSE']), 0) == 14 and float(row['ACTUALFRACTIONS']) == 4) or (round(float(row['RTACTUALDOSE']), 0) == 40 and float(row['ACTUALFRACTIONS']) == 10) or (round(float(row['RTACTUALDOSE']), 0) == 37 and float(row['ACTUALFRACTIONS']) == 13) or (round(float(row['RTACTUALDOSE']), 0) == 38 and float(row['ACTUALFRACTIONS']) == 13)):
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], 'Palliative'))

        ####

        # Oesophagus cancer and:
        # - 12 in 1#
        # - 12 in 2#
        # - 16 in 2#
        # - 35 in 15#
        # - 40 in 15#
        # => Palliative
        elif row['RADIOTHERAPYDIAGNOSISICD'] == 'C15' and ((round(float(row['RTACTUALDOSE']), 0) == 12 and float(row['ACTUALFRACTIONS']) == 1) or (round(float(row['RTACTUALDOSE']), 0) == 12 and float(row['ACTUALFRACTIONS']) == 2) or (round(float(row['RTACTUALDOSE']), 0) == 16 and float(row['ACTUALFRACTIONS']) == 2) or (round(float(row['RTACTUALDOSE']), 0) == 35 and float(row['ACTUALFRACTIONS']) == 15) or (round(float(row['RTACTUALDOSE']), 0) == 40 and float(row['ACTUALFRACTIONS']) == 15)):
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], 'Palliative'))

        ####

        # Colorectal cancer and:
        # - 12 in 2#
        # - 5.4 in 3# or
        # - 20 in 4# or
        # - 25 in 5#
        # - 45 in 25# or
        # - 50.4 in 28
        # => Not palliative
        elif row['RADIOTHERAPYDIAGNOSISICD'] == 'C20' and ((round(float(row['RTACTUALDOSE']), 0) == 12 and float(row['ACTUALFRACTIONS']) == 2) or (round(float(row['RTACTUALDOSE']), 0) in [5, 5.4] and float(row['ACTUALFRACTIONS']) == 3) or (round(float(row['RTACTUALDOSE']), 0) == 20 and float(row['ACTUALFRACTIONS']) == 4) or (round(float(row['RTACTUALDOSE']), 0) in [25, 26] and float(row['ACTUALFRACTIONS']) == 5) or (round(float(row['RTACTUALDOSE']), 0) == 45 and float(row['ACTUALFRACTIONS']) == 25) or (round(float(row['RTACTUALDOSE']), 0) in [50, 50.4] and float(row['ACTUALFRACTIONS']) == 28)):
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], 'Not Palliative'))

        ####

        # Anal cancer and:
        # - 20 in 10#
        # => Palliative
        elif row['RADIOTHERAPYDIAGNOSISICD'] == 'C21' and (round(float(row['RTACTUALDOSE']), 0) == 20 and float(row['ACTUALFRACTIONS']) == 10):
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], 'Palliative'))

        ####

        # Anal cancer and:
        # - 40 in 28#
        # - 50.4 in 28#
        # - 53.2 in 28#
        # => Not Palliative
        elif row['RADIOTHERAPYDIAGNOSISICD'] == 'C21' and ((round(float(row['RTACTUALDOSE']), 0) == 40 and float(row['ACTUALFRACTIONS']) == 28) or (round(float(row['RTACTUALDOSE']), 0) in [50, 50.4] and float(row['ACTUALFRACTIONS']) == 28) or (round(float(row['RTACTUALDOSE']), 0) in [53, 53.2] and float(row['ACTUALFRACTIONS']) == 28)):
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], 'Not Palliative'))

        ###

        # Lung cancer and treated in the brain (Z01) and:
        # - 22 in 9# or
        # - 23 in 9#
        # - 25 in 10#
        # => Palliative
        elif row['RADIOTHERAPYDIAGNOSISICD'] == 'C34' and 'Z01' in row['RTTREATMENTANATOMICALSITE'] and ((round(float(row['RTACTUALDOSE']), 0) == 25 and float(row['ACTUALFRACTIONS']) == 10) or (round(float(row['RTACTUALDOSE']), 0) in [22, 23] and float(row['ACTUALFRACTIONS']) == 9)):
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], 'Palliative'))

        ####

        # Lung cancer or treated for a lung metastasis and:
        # - 17 in 2#
        # - 25 in 10# or
        # - 24 in 12# or
        # - less than 30Gy and Z01 not in AttributeValue
        # => Palliative
        elif (row['RADIOTHERAPYDIAGNOSISICD'] == 'C34' or 'Z246' in row['RTTREATMENTANATOMICALSITE']) and ((round(float(row['RTACTUALDOSE']), 0) == 10 and float(row['ACTUALFRACTIONS']) == 1) or (round(float(row['RTACTUALDOSE']), 0) == 17 and float(row['ACTUALFRACTIONS']) == 2) or (round(float(row['RTACTUALDOSE']), 0) == 25 and float(row['ACTUALFRACTIONS']) == 10) or (float(row['RTACTUALDOSE'] == 24) and float(row['ACTUALFRACTIONS']) == 12) or float(row['RTACTUALDOSE']) < 30 and 'Z01' not in row['RTTREATMENTANATOMICALSITE']):
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], 'Palliative'))

        ####

        # Lung cancer or treated for a lung metastasis and:
        # - 54 in 3# or
        # - 55 in 5# or
        # - 60 in 5# or
        # - 60 in 8# or
        # - more than 50 Gy and # < 10 and dose/# >= 7
        # => Not palliative (SABR)
        elif (row['RADIOTHERAPYDIAGNOSISICD'] == 'C34' or 'Z246' in row['RTTREATMENTANATOMICALSITE']) and ((round(float(row['RTACTUALDOSE']), 0) == 54 and float(row['ACTUALFRACTIONS']) == 3) or (round(float(row['RTACTUALDOSE']), 0) == 55 and float(row['ACTUALFRACTIONS']) == 5) or (round(float(row['RTACTUALDOSE']), 0) == 60 and float(row['ACTUALFRACTIONS']) in [5, 8]) or float(row['RTACTUALDOSE']) >= 50 and float(row['ACTUALFRACTIONS']) < 10 and float(row['RTACTUALDOSE'])/float(row['ACTUALFRACTIONS']) >= 7):
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], 'Not Palliative'))

        ####

        # Lung cancer and:
        # - more than 50 Gy and # >= 15 and dose/# >= 3
        # => Not palliative (Radical)
        elif row['RADIOTHERAPYDIAGNOSISICD'] == 'C34' and (float(row['RTACTUALDOSE']) >= 50 and float(row['ACTUALFRACTIONS']) >= 15 and float(row['RTACTUALDOSE'])/float(row['ACTUALFRACTIONS']) >= 3):
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], 'Not Palliative'))

        ####

        # Skin cancer and:
        # - 10 in 1# --> useless?
        # => Palliative
        elif (row['RADIOTHERAPYDIAGNOSISICD'] in ['C43', 'C44']) and (round(float(row['RTACTUALDOSE']), 0) == 10 and float(row['ACTUALFRACTIONS']) == 1):
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], 'Palliative'))

        ####

        # Skin cancer and:
        # - 18 in 1# or
        # - 28 in 4# (35 in 5# not finished) or
        # - 32 in 4#
        # - 33 in 4# or
        # - 35 in 5# or
        # - 38 in 8# or
        # - 40 in 8# or
        # - 40 in 9# (45 in 10# not finished) or
        # - 41 in 9# (45 in 10# not finished) or
        # - 40 in 10# or
        # - 40 in 15# or
        # - 45 in 10# or
        # - 50 in 15# or
        # - 60 in 20#
        # => Not palliative
        elif (row['RADIOTHERAPYDIAGNOSISICD'] in ['C43', 'C44']) and ((round(float(row['RTACTUALDOSE']), 0) == 18 and float(row['ACTUALFRACTIONS']) == 1) or (round(float(row['RTACTUALDOSE']), 0) in [28, 32, 33] and float(row['ACTUALFRACTIONS']) == 4) or (round(float(row['RTACTUALDOSE']), 0) == 35 and float(row['ACTUALFRACTIONS']) == 5) or (round(float(row['RTACTUALDOSE']), 0) in [38, 40] and float(row['ACTUALFRACTIONS']) == 8) or (round(float(row['RTACTUALDOSE']), 0) in [40, 41] and float(row['ACTUALFRACTIONS']) == 9) or (round(float(row['RTACTUALDOSE']), 0) in [40, 45] and float(row['ACTUALFRACTIONS']) == 10) or (round(float(row['RTACTUALDOSE']), 0) in [40, 50] and float(row['ACTUALFRACTIONS']) == 15) or (round(float(row['RTACTUALDOSE']), 0) == 60 and float(row['ACTUALFRACTIONS']) == 20)):
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], 'Not Palliative'))

        ####

        # Breast cancer and (the boosts):
        # - 9 in 3#
        # - 10 in 3#
        # - 11 in 3#
        # - 8 in 4#
        # - 10 in 5#
        # - 14 in 5#
        # - 15 in 5#
        # - 16 in 8#
        # - 20 in 10#
        # - 40 in 15# with Z15 in anatomical site
        # => Not Palliative
        elif row['RADIOTHERAPYDIAGNOSISICD'] == 'C50' and row['RTTREATMENTREGION'] != 'M' and ((round(float(row['RTACTUALDOSE']), 0) in [9, 10, 11] and float(row['ACTUALFRACTIONS']) == 3) or (round(float(row['RTACTUALDOSE']), 0) == 8 and float(row['ACTUALFRACTIONS']) == 4) or (round(float(row['RTACTUALDOSE']), 0) in [10, 13, 14, 15] and float(row['ACTUALFRACTIONS']) == 5) or (round(float(row['RTACTUALDOSE']), 0) == 16 and float(row['ACTUALFRACTIONS']) == 8) or (round(float(row['RTACTUALDOSE']), 0) == 20 and float(row['ACTUALFRACTIONS']) == 10) or (round(float(row['RTACTUALDOSE']), 0) == 40 and float(row['ACTUALFRACTIONS']) == 15 and 'Z15' in row['RTTREATMENTANATOMICALSITE'])):
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], 'Not Palliative'))

        ####

        # Breast cancer and 36 in 6#
        # => Palliative
        elif row['RADIOTHERAPYDIAGNOSISICD'] == 'C50' and (round(float(row['RTACTUALDOSE']), 0) == 36 and float(row['ACTUALFRACTIONS']) == 6):
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], 'Palliative'))

        ####

        # # Brachytherapy machine used for cervix cancer
        # # => Not palliative
        # elif row[5] == 'C53' and (row[11] == '06' or int(row[11]) == 6):
        #   pallia.append((row[0], row[1], float(row[2]), float(row[3]), row[4], row[5], row[6], row[7], row[8], row[9], row[10], row[11], 'Not Palliative'))

        # ####

        # # Gynae cancer and:
        # # - 22 in 4# or
        # # - 21 in 3#
        # # => Not palliative
        # elif row[5] in ['C51', 'C52', 'C53', 'C54', 'C55', 'C56', 'C57'] and ((float(row[2]) == 22 and float(row[3]) == 4) or (float(row[2]) == 21 and float(row[3]) == 3)):
        #   pallia.append((row[0], row[1], float(row[2]), float(row[3]), row[4], row[5], row[6], row[7], row[8], row[9], row[10], row[11], 'Not Palliative'))

        # ####

        # # Gynea cancer and 30 in 10#
        # # => Palliative
        # elif row[5] in ['C51', 'C52', 'C53', 'C54', 'C55', 'C56', 'C57'] and (float(row[2]) == 30 and float(row[3]) == 10):
        #   pallia.append((row[0], row[1], float(row[2]), float(row[3]), row[4], row[5], row[6], row[7], row[8], row[9], row[10], row[11], 'Palliative'))

        ####

        # Cervix cancer and:
        # - 22 in 4# and brachy or
        # - brachy
        # => Not palliative
        elif row['RADIOTHERAPYDIAGNOSISICD'] == 'C53' and ((round(float(row['RTACTUALDOSE']), 0) == 22 and float(row['ACTUALFRACTIONS']) == 4 and (row['RTTREATMENTMODALITY'] == '06' or int(row['RTTREATMENTMODALITY']) == 6)) or (row['RTTREATMENTMODALITY'] == '06' or int(row['RTTREATMENTMODALITY']) == 6)):
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], 'Not Palliative'))

        ####

        # Endometrial cancer and:
        # - 8 in 2# (and brachy) or
        # - 21 in 3# (and brachy) or
        # - 28 in 4# (and brachy) or
        # - 25 in 5# (and brachy) or
        # - 36 in 5# (and brachy) or
        # - 37.5 in 6# (and brachy) or
        # - from 12 to 30 in 3-8# (and brachy)
        # => Not palliative
        elif row['RADIOTHERAPYDIAGNOSISICD'] == 'C54' and (row['RTTREATMENTMODALITY'] == '06' or int(row['RTTREATMENTMODALITY']) == 6) and ((round(float(row['RTACTUALDOSE']), 0) == 8 and float(row['ACTUALFRACTIONS']) == 2) or (round(float(row['RTACTUALDOSE']), 0) == 21 and float(row['ACTUALFRACTIONS']) == 3) or (round(float(row['RTACTUALDOSE']), 0) == 28 and float(row['ACTUALFRACTIONS']) == 4) or (round(float(row['RTACTUALDOSE']), 0) == 25 and float(row['ACTUALFRACTIONS']) == 5) or (round(float(row['RTACTUALDOSE']), 0) == 25 and float(row['ACTUALFRACTIONS']) == 5) or (round(float(row['RTACTUALDOSE']), 0) == 36 and float(row['ACTUALFRACTIONS']) == 5) or (round(float(row['RTACTUALDOSE']), 0) in [27.5, 38] and float(row['ACTUALFRACTIONS']) == 6) or (round(float(row['RTACTUALDOSE']), 0) in range(12, 31, 1) and float(row['ACTUALFRACTIONS']) in range(3, 9, 1))):
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], 'Not Palliative'))

        ####

        # Prostate cancer and 8 in 1# and breast treated
        # => Not Palliative
        elif row['RADIOTHERAPYDIAGNOSISICD'] == 'C61' and row['RTTREATMENTANATOMICALSITE'][0:3] == 'Z15' and (round(float(row['RTACTUALDOSE']), 0) == 8 and float(row['ACTUALFRACTIONS']) == 1):
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], 'Not Palliative'))

        ####

        # Prostate cancer and:
        # - 8 in 1#
        # - 10 in 1#
        # => Palliative
        elif row['RADIOTHERAPYDIAGNOSISICD'] == 'C61' and row['RTTREATMENTANATOMICALSITE'][0:3] != 'Z15' and (round(float(row['RTACTUALDOSE']), 0) in [8, 10] and float(row['ACTUALFRACTIONS']) == 1):
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], 'Palliative'))

        ####

        # Prostate cancer and:
        # - 60 in 20#
        # - 61 in 20#
        # => Not Palliative
        elif row['RADIOTHERAPYDIAGNOSISICD'] == 'C61' and row['RTTREATMENTANATOMICALSITE'][0:3] != 'Z15' and (round(float(row['RTACTUALDOSE']), 0) in [60, 61] and float(row['ACTUALFRACTIONS']) == 20):
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], 'Not Palliative'))

        ####

        # Bladder cancer and 36 in 6#
        # => Palliative
        elif row['RADIOTHERAPYDIAGNOSISICD'] == 'C67' and (round(float(row['RTACTUALDOSE']), 0) == 36 and float(row['ACTUALFRACTIONS']) == 6):
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], 'Palliative'))

        ####

        # (Brain tumour or spinal cord) and:
        # - 30 in 6# or
        # - 37 in 13# or
        # - 34 in 10# or
        # - 40 in 15#
        # => Palliative
        elif row['RADIOTHERAPYDIAGNOSISICD'] in ['C70', 'C71', 'C72'] and ((round(float(row['RTACTUALDOSE']), 0) == 30 and float(row['ACTUALFRACTIONS']) == 6) or (round(float(row['RTACTUALDOSE']), 0) == 37 and float(row['ACTUALFRACTIONS']) == 13) or (round(float(row['RTACTUALDOSE']), 0) == 34 and float(row['ACTUALFRACTIONS']) == 10) or (round(float(row['RTACTUALDOSE']), 0) == 40 and float(row['ACTUALFRACTIONS']) == 15)):
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], 'Palliative'))

        ####

        # Brain tumour and 45 in 15#
        # => Not palliative
        elif row['RADIOTHERAPYDIAGNOSISICD'] == 'C71' and (round(float(row['RTACTUALDOSE']), 0) == 45 and float(row['ACTUALFRACTIONS']) == 15):
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], 'Not Palliative'))

        ####

        # Treated for brain mets ("Z01") but with another diagnosis than Brain Tumor ("C71") and:
        # - 30 in 10# or
        # - 25 in 10# or
        # - 20 in 5# or
        # - 12 in 2#
        # => Palliative
        elif row['RADIOTHERAPYDIAGNOSISICD'] != "C71" and 'Z01' in row['RTTREATMENTANATOMICALSITE'] and ((round(float(row['RTACTUALDOSE']), 0) in [25, 30] and float(row['ACTUALFRACTIONS']) == 10) or (round(float(row['RTACTUALDOSE']), 0) == 20 and float(row['ACTUALFRACTIONS']) == 5) or (round(float(row['RTACTUALDOSE']), 0) == 12 and float(row['ACTUALFRACTIONS']) == 2)):
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], 'Palliative'))

        ####

        # "Spleen" (both combined):
        # - 0.5Gy/#
        # - "Spleen" (Z313) in anatomical site
        # => Palliative
        elif round(float(row['RTACTUALDOSE'])/float(row['ACTUALFRACTIONS']), 1) == 0.5 and row['RTTREATMENTANATOMICALSITE'] in ['Z313', 'Z31']:
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], 'Palliative'))

        ####

        # Treated for brain mets ("Z01") and fraction(s) between 1# and 5# and dose per fraction > 6 but dose different than 8 (to avoid including "8 in 1#" as a SRS dose)
        # or
        # Treated for a primary site ("Primary") and dose per fraction > 5 but dose different than 8 (to avoid including "8 in 1#" as a SRS dose)
        # => SRS
        elif ('Z01' in row['RTTREATMENTANATOMICALSITE'] and float(row['ACTUALFRACTIONS']) in [1, 2, 3, 4, 5] and float(row['RTACTUALDOSE'])/float(row['ACTUALFRACTIONS']) > 6) or (row['RTTREATMENTANATOMICALSITE'] == 'C71' and row['RTTREATMENTREGION'] == 'P' and float(row['RTACTUALDOSE'])/float(row['ACTUALFRACTIONS']) > 5):
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], 'Not Palliative'))

        ####

        # # Cyberknife treatments to mets
        # # => Not palliative
        # elif 'M' in row[5] and (float(row[3]) in range(30, 39, 1) or float(row[3]) in [3, 5]):
        #   pallia.append((row[0], row[1], float(row[2]), float(row[3]), row[4], row[5], row[6], row[7], row[8], row[9], row[10], row[11], row[12], 'Not Palliative'))

        ####

        # 3 <= Dose per fraction <= 10
        # => Pallative
        elif float(row['RTACTUALDOSE'])/float(row['ACTUALFRACTIONS']) >= 3 and float(row['RTACTUALDOSE'])/float(row['ACTUALFRACTIONS']) <= 10:
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], 'Palliative'))

        ####

        # Treated for a metastasis
        elif row['RTTREATMENTREGION'] == 'M':
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], 'Palliative'))

        ####

        # => Not Palliative
        else:
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], 'Not Palliative'))

    return(pallia)


# #################################################### #


# ##################################### #
# Functions for the NatCanSat algorithm #
# ##################################### #


def NatCanSat(list_raw):
    pallia = []

    for row in list_raw:

        # 1. Episodes containing a prescription with the region coded as metastatic
        # Palliative
        if row['RTTREATMENTREGION'] == "M":
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], row['ValuePredicted_RuleBased'], row['pred.bn'], 'Palliative'))

        # 2. Brachytherapy episodes are RADICAL (0.5%), except for episodes with a primary diagnosis of lung (C34), mesothelioma (C45) or oesophagus (C15) cancer
        # Radical
        elif (row['RTTREATMENTMODALITY'] == '06' or int(row['RTTREATMENTMODALITY']) == 6) and row['RADIOTHERAPYDIAGNOSISICD'][0:3] not in ['C34', 'C45', 'C15']:
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], row['ValuePredicted_RuleBased'], row['pred.bn'], 'Not Palliative'))

        # 3. Episodes containing a prescription of 15 or more fractions
        # Radical
        elif float(row['ACTUALFRACTIONS']) >= 15:
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], row['ValuePredicted_RuleBased'], row['pred.bn'], 'Not Palliative'))

        # 4. Episodes with a non cancer primary diagnosis (Arterio - venous malformation and Benign disease e.g. Endocrine, skin)
        # Radical
        elif row['RADIOTHERAPYDIAGNOSISICD'][0] != 'C':
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], row['ValuePredicted_RuleBased'], row['pred.bn'], 'Not Palliative'))

        # 5. Primary breast cancer (C50 & D05) episodes with prescription of prescribed dose 39, 40 or 41.6 Gy with 12 or 13 fractions and a treatment region coded P, R or PR (Hypofractionated breast)
        # Radical
        elif (row['RADIOTHERAPYDIAGNOSISICD'][0:3] == 'C50' or row['RADIOTHERAPYDIAGNOSISICD'][0:3] == 'D05') and round(float(row['PRESCRIBEDDOSE']), 1) in [39.0, 40.0, 41.6] and float(row['ACTUALFRACTIONS']) in [12, 13] and (row['RTTREATMENTREGION'] == 'P' or row['RTTREATMENTREGION'] == 'R' or row['RTTREATMENTREGION'] == 'PR'):
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], row['ValuePredicted_RuleBased'], row['pred.bn'], 'Not Palliative'))

        # 6. Episodes for primary lung cancer (C34) which containing a prescription with the region coded as prophylactic and treatment site coded as brain (Z01) (Prophylactic brain irradiation (PCI) for small cell lung cancer)
        # Radical
        elif row['RADIOTHERAPYDIAGNOSISICD'][0:3] == 'C34' and row['RTTREATMENTREGION'] == 'O' and 'Z01' in row['RTTREATMENTANATOMICALSITE']:
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], row['ValuePredicted_RuleBased'], row['pred.bn'], 'Not Palliative'))

        # 7. Episodes with a prescription coded with a region as primary or regional nodes, with a high dose, low fractionation and treated on a linear accelerator with stereotactic capabilities (Stereotactic Ablative, SABR and Brain radiosurgery)
        # Radical
        elif (row['RTTREATMENTREGION'] == 'P' or row['RTTREATMENTREGION'] == 'PR') and ((float(row['RTACTUALDOSE']) == 54 and float(row['ACTUALFRACTIONS']) == 3) or (float(row['RTACTUALDOSE']) == 55 and float(row['ACTUALFRACTIONS']) == 5) or (float(row['RTACTUALDOSE']) == 60 and float(row['ACTUALFRACTIONS']) == 8) or (float(row['RTACTUALDOSE']) == 60 and float(row['ACTUALFRACTIONS']) == 3) or (float(row['RTACTUALDOSE']) == 48 and float(row['ACTUALFRACTIONS']) == 4) or (float(row['RTACTUALDOSE']) == 18 and float(row['ACTUALFRACTIONS']) == 1)):
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], row['ValuePredicted_RuleBased'], row['pred.bn'], 'Not Palliative'))

        # 8. Episodes for testicular cancer (C62) with a prescription coded with a region as primary or regional nodes (Testicular para-aortic or dogleg)
        # Radical
        elif row['RADIOTHERAPYDIAGNOSISICD'][0:3] == 'C62' and (row['RTTREATMENTREGION'] == 'P' or row['RTTREATMENTREGION'] == 'PR'):
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], row['ValuePredicted_RuleBased'], row['pred.bn'], 'Not Palliative'))

        # 9. Episodes for rectal cancer (C19, C20 & C21) with a prescription coded with a region as primary and a dose fractionation of 25Gy in 5# (Preoperative RT for rectal cancer)
        # Radical
        elif (row['RADIOTHERAPYDIAGNOSISICD'][0:3] in ['C19', 'C20', 'C21']) and row['RTTREATMENTREGION'] == 'P' and float(row['RTACTUALDOSE']) == 25 and float(row['ACTUALFRACTIONS']) == 5:
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], row['ValuePredicted_RuleBased'], row['pred.bn'], 'Not Palliative'))

        # 10. Episodes for skin cancer (C43) with a prescription coded with a region as primary (Non-melanoma skin cancer)
        # Radical
        elif row['RADIOTHERAPYDIAGNOSISICD'][0:3] == 'C43' and row['RTTREATMENTREGION'] == 'P':
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], row['ValuePredicted_RuleBased'], row['pred.bn'], 'Not Palliative'))

        # 11. Episodes for breast cancer (C50 & D05) with a prescription coded with a region as prophylactic and treatment site of pelvis (Z75) (Artificial menopause)
        # Radical
        elif (row['RADIOTHERAPYDIAGNOSISICD'][0:3] in ['C50', 'D05']) and row['RTTREATMENTREGION'] == 'O' and 'Z75' in row['RTTREATMENTANATOMICALSITE']:
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], row['ValuePredicted_RuleBased'], row['pred.bn'], 'Not Palliative'))

        # 12. Episodes for prostate cancer (C61) with a prescription coded with a region as prophylactic and treatment site of breast (Z15) (<0.1%) (Breast irradiation for 60Gy in 8#; gynaecomastia)
        # Radical
        elif row['RADIOTHERAPYDIAGNOSISICD'][0:3] == 'C61' and row['RTTREATMENTREGION'] == 'O' and 'Z15' in row['RTTREATMENTANATOMICALSITE']:
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], row['ValuePredicted_RuleBased'], row['pred.bn'], 'Not Palliative'))

        # All remaining episodes are PALLIATIVE (26%)
        else:
            pallia.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), row['TREATMENTSTARTDATE'], row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE'], row['ValuePredicted_SVM'], row['ValuePredicted_RuleBased'], row['pred.bn'], 'Palliative'))

        # Stereotactic dose fractionations seen are:
        # 54Gy in 3#; 55Gy in 5#; 60Gy in 8#; 60Gy in 3#; 48Gy in 4# and 18Gy in 1#

        ####

    return(pallia)


# #################################################### #


# ##################### #
# Functions for the SVM #
# ##################### #


# To clean the csv files
def clean_csv(dataset):
    dataset.fillna(" ", inplace=True)
    dataset['GOLDVALUE'].replace(to_replace='Not palliative', value='Not Palliative', inplace=True)
    return(dataset)


# ###


# To label a string into an integer: http://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.LabelEncoder.html
def LabelString(data, column):
    le = preprocessing.LabelEncoder()
    le.fit(data.iloc[:, column])
    data.iloc[:, column] = le.transform(data.iloc[:, column])
    return(data)


# ###


# Label the whole dataset for each column
def WholeLabelString(data):
    # Change the RTActualDose
    LabelString(data, 0)
    # Change the ActualFractions
    LabelString(data, 1)
    # Change the RadiotherapyDiagnosisICD
    LabelString(data, 2)
    # Change the RTTreatmentRegion
    LabelString(data, 3)
    # Change the RTTreatmentAnatomicalSite
    LabelString(data, 4)
    # Change the PrescribedDose
    LabelString(data, 5)
    # Change the PrescribedFractions
    LabelString(data, 6)
    # Change the RTTreatmentModality
    LabelString(data, 7)
    # Change the GoldStandard
    LabelString(data, 8)


# ###


# Measure the performance of the classifier using the confusion matrix
def PerfMeasure(y_true, y_pred):
    cm = confusion_matrix(y_true, y_pred, labels=[1, 0])
    TP = cm[0, 0]
    FN = cm[0, 1]
    FP = cm[1, 0]
    TN = cm[1, 1]

    accuracy = round(float((TP+TN)/(TP+TN+FP+FN)), 3)
    sensitivity = round(float(TP/(TP+FN)), 3)
    specificity = round(float(TN/(TN+FP)), 3)
    precision = round(float(TP/(TP+FP)), 3)
    f1score = round(float((2*sensitivity*precision)/(sensitivity+precision)), 3)

    return("accuracy: ", accuracy, "sensitivity: ", sensitivity, "specificity: ", specificity, "precision: ", precision, "F1-score: ", f1score)


# ###


# Function to concatenate the two or three datasets
def gather(X, Y, DatasetIDs, A_Dose, A_Fracs, ICD, Region, Site, P_Dose, P_Fracs, Modality, Gold, dataset, **Prediction):
    # For the training dataset
    if dataset == 'train':
        # Concatenate the data, the gold value and the value predicted
        X = pd.DataFrame(data=X[:])
        X = X.rename(columns={0: 'Indices', 1: 'RTACTUALDOSE', 2: 'ACTUALFRACTIONS', 3: 'RADIOTHERAPYDIAGNOSISICD', 4: 'RTTREATMENTREGION', 5: 'RTTREATMENTANATOMICALSITE', 6: 'PRESCRIBEDDOSE', 7: 'PRESCRIBEDFRACTIONS', 8: 'RTTREATMENTMODALITY', 9: 'GOLDVALUE'})

        Y = pd.DataFrame(Y[:])
        Y = Y.rename(columns={0: 'Indices', 1: 'GOLDVALUE'})

        result_predict = pd.merge(X, Y, how='left', on='Indices')

        # Merge the real values to the preprocessed values
        result_1 = pd.merge(result_predict, A_Dose, how='left', on='RTACTUALDOSE')
        result_2 = pd.merge(result_1, A_Fracs, how='left', on='ACTUALFRACTIONS')
        result_3 = pd.merge(result_2, ICD, how='left', on='RADIOTHERAPYDIAGNOSISICD')
        result_4 = pd.merge(result_3, Region, how='left', on='RTTREATMENTREGION')
        result_5 = pd.merge(result_4, Site, how='left', on='RTTREATMENTANATOMICALSITE')
        result_6 = pd.merge(result_5, P_Dose, how='left', on='PRESCRIBEDDOSE')
        result_7 = pd.merge(result_6, P_Fracs, how='left', on='PRESCRIBEDFRACTIONS')
        result_8 = pd.merge(result_7, Modality, how='left', on='RTTREATMENTMODALITY')
        result = pd.merge(result_8, Gold, how='left', on='GOLDVALUE')

        # Keep the columns with the real values
        result = result[['Indices', 'RTActualDose_keep', 'ActualFractions_keep', 'RadiotherapyDiagnosisICD_keep', 'RTTreatmentRegion_keep', 'RTTreatmentAnatomicalSite_keep', 'PrescribedDose_keep', 'PrescribedFractions_keep', 'RTTreatmentModality_keep', 'GoldValue_keep']]

        # Rename the columns
        result = result.rename(columns={'Indices': 'Indices', 'RTActualDose_keep': 'RTACTUALDOSE', 'ActualFractions_keep': 'ACTUALFRACTIONS', 'RadiotherapyDiagnosisICD_keep': 'RADIOTHERAPYDIAGNOSISICD', 'RTTreatmentRegion_keep': 'RTTREATMENTREGION', 'RTTreatmentAnatomicalSite_keep': 'RTTREATMENTANATOMICALSITE', 'PrescribedDose_keep': 'PRESCRIBEDDOSE', 'PrescribedFractions_keep': 'PRESCRIBEDFRACTIONS', 'RTTreatmentModality_keep': 'RTTREATMENTMODALITY', 'GoldValue_keep': 'GOLDVALUE'})

        result_temp = pd.merge(result, DatasetIDs, how='left', on='Indices')

        result_final = result_temp[['ORGCODEPROVIDER', 'NHSNO', 'RTACTUALDOSE', 'ACTUALFRACTIONS', 'TREATMENTSTARTDATE', 'RADIOTHERAPYDIAGNOSISICD', 'RTTREATMENTREGION', 'RTTREATMENTANATOMICALSITE', 'PRESCRIBEDDOSE', 'PRESCRIBEDFRACTIONS', 'RADIOTHERAPYINTENT', 'RTTREATMENTMODALITY', 'GOLDVALUE']]

        return(result_final)

    # For the test dataset
    elif dataset == 'test':
        # Copy the Gold dataset to merge the values from the GoldStandard to the prediction
        Value = Gold.copy()
        Value = Value.rename(columns={'GoldValue_keep': 'ValuePredicted_SVM_keep', 'GOLDVALUE': 'ValuePredicted_SVM'})

        # Concatenate the data, the gold value and the value predicted
        X = pd.DataFrame(data=X[:])
        X = X.rename(columns={0: 'Indices', 1: 'RTACTUALDOSE', 2: 'ACTUALFRACTIONS', 3: 'RADIOTHERAPYDIAGNOSISICD', 4: 'RTTREATMENTREGION', 5: 'RTTREATMENTANATOMICALSITE', 6: 'PRESCRIBEDDOSE', 7: 'PRESCRIBEDFRACTIONS', 8: 'RTTREATMENTMODALITY', 9: 'GOLDVALUE'})

        Y = pd.DataFrame(Y[:])
        Y = Y.rename(columns={0: 'Indices', 1: 'GOLDVALUE'})

        Prediction = pd.DataFrame(Prediction)
        Prediction = Prediction.rename(columns={'Prediction': 'ValuePredicted_SVM'})

        Y_and_Prediction = pd.concat([Y, Prediction], axis=1)

        result_predict = pd.merge(X, Y_and_Prediction, how='left', on='Indices')

        # Merge the real values to the preprocessed values
        result_1 = pd.merge(result_predict, A_Dose, how='left', on='RTACTUALDOSE')
        result_2 = pd.merge(result_1, A_Fracs, how='left', on='ACTUALFRACTIONS')
        result_3 = pd.merge(result_2, ICD, how='left', on='RADIOTHERAPYDIAGNOSISICD')
        result_4 = pd.merge(result_3, Region, how='left', on='RTTREATMENTREGION')
        result_5 = pd.merge(result_4, Site, how='left', on='RTTREATMENTANATOMICALSITE')
        result_6 = pd.merge(result_5, P_Dose, how='left', on='PRESCRIBEDDOSE')
        result_7 = pd.merge(result_6, P_Fracs, how='left', on='PRESCRIBEDFRACTIONS')
        result_8 = pd.merge(result_7, Modality, how='left', on='RTTREATMENTMODALITY')
        result_9 = pd.merge(result_8, Gold, how='left', on='GOLDVALUE')
        result = pd.merge(result_9, Value, how='left', on='ValuePredicted_SVM')

        # Keep the columns with the real values
        result = result[['Indices', 'RTActualDose_keep', 'ActualFractions_keep', 'RadiotherapyDiagnosisICD_keep', 'RTTreatmentRegion_keep', 'RTTreatmentAnatomicalSite_keep', 'PrescribedDose_keep', 'PrescribedFractions_keep', 'RTTreatmentModality_keep', 'GoldValue_keep', 'ValuePredicted_SVM_keep']]

        # Rename the columns
        result = result.rename(columns={'Indices': 'Indices', 'RTActualDose_keep': 'RTACTUALDOSE', 'ActualFractions_keep': 'ACTUALFRACTIONS', 'RadiotherapyDiagnosisICD_keep': 'RADIOTHERAPYDIAGNOSISICD', 'RTTreatmentRegion_keep': 'RTTREATMENTREGION', 'RTTreatmentAnatomicalSite_keep': 'RTTREATMENTANATOMICALSITE', 'PrescribedDose_keep': 'PRESCRIBEDDOSE', 'PrescribedFractions_keep': 'PRESCRIBEDFRACTIONS', 'RTTreatmentModality_keep': 'RTTREATMENTMODALITY', 'GoldValue_keep': 'GOLDVALUE', 'ValuePredicted_SVM_keep': 'ValuePredicted_SVM'})

        result_temp = pd.merge(result, DatasetIDs, how='left', on='Indices')

        result_final = result_temp[['ORGCODEPROVIDER', 'NHSNO', 'RTACTUALDOSE', 'ACTUALFRACTIONS', 'TREATMENTSTARTDATE', 'RADIOTHERAPYDIAGNOSISICD', 'RTTREATMENTREGION', 'RTTREATMENTANATOMICALSITE', 'PRESCRIBEDDOSE', 'PRESCRIBEDFRACTIONS', 'RADIOTHERAPYINTENT', 'RTTREATMENTMODALITY', 'GOLDVALUE', 'ValuePredicted_SVM']]

        return(result_final)


# #################################################### #


# ####################################### #
# Functions to add the treatment end date #
# ####################################### #


# To count the number of working/business day
def date_by_adding_business_days(from_date, add_days):
    business_days_to_add = int(add_days)
    current_date = datetime.datetime.strptime(from_date, "%d-%b-%y")
    while business_days_to_add > 0:
        current_date += datetime.timedelta(days=1)
        weekday = current_date.weekday()
        if weekday >= 5:  # sunday = 6
            continue
        business_days_to_add -= 1
    return(current_date)


def _in_between(a, b, x):
    return(a <= x <= b or b <= x <= a)


def cmp(a, b):
    return((a > b) - (a < b))


def workday(start_date, days, holidays, weekends):
    if days == 0:
        return(start_date)
    if days > 0 and start_date.weekday() in weekends:
        while start_date.weekday() in weekends:
          start_date -= datetime.timedelta(days=1)
    elif days < 0:
        while start_date.weekday() in weekends:
          start_date += datetime.timedelta(days=1)
    full_weeks, extra_days = divmod(days, 7 - len(weekends))
    new_date = start_date + datetime.timedelta(weeks=full_weeks)
    for i in range(extra_days):
        new_date += datetime.timedelta(days=1)
        while new_date.weekday() in weekends:
            new_date += datetime.timedelta(days=1)
    # to account for days=0 case
    while new_date.weekday() in weekends:
        new_date += datetime.timedelta(days=1)

    # avoid this if no holidays
    if holidays:
        delta = datetime.timedelta(days=1 * cmp(days, 0))
        # skip holidays that fall on weekends
        holidays = [x for x in holidays if x.weekday() not in weekends]
        holidays = [x for x in holidays if x != start_date]
        for d in sorted(holidays, reverse=(days < 0)):
            # if d in between start and current push it out one working day
            if _in_between(start_date, new_date, d):
                new_date += delta
                while new_date.weekday() in weekends:
                    new_date += delta
    return(new_date)


def AddEndDate(list_raw):
    # Define the weekday mnemonics to match the date.weekday function
    (MON, TUE, WED, THU, FRI, SAT, SUN) = range(7)

    # Define default weekends, but it allows to be overridden at the function level in case someone only, for example, only has a 4-day workweek.
    default_weekends = (SAT, SUN)

    holidays = [datetime.datetime(2016, 1, 1), datetime.datetime(2016, 3, 25), datetime.datetime(2016, 3, 28), datetime.datetime(2016, 5, 2), datetime.datetime(2016, 5, 30), datetime.datetime(2016, 8, 29), datetime.datetime(2016, 12, 26), datetime.datetime(2016, 12, 27), datetime.datetime(2017, 1, 2), datetime.datetime(2017, 4, 14), datetime.datetime(2017, 4, 17), datetime.datetime(2017, 5, 1), datetime.datetime(2017, 5, 29), datetime.datetime(2017, 8, 28), datetime.datetime(2017, 12, 25), datetime.datetime(2017, 12, 26)]

    end_ttt = []

    for row in list_raw:
        end_ttt.append((row['ORGCODEPROVIDER'], row['NHSNO'], float(row['RTACTUALDOSE']), float(row['ACTUALFRACTIONS']), datetime.datetime.strptime(row['TREATMENTSTARTDATE'], "%d/%m/%Y"), workday(start_date=datetime.datetime.strptime(row['TREATMENTSTARTDATE'], "%d/%m/%Y"), days=int(int(row['ACTUALFRACTIONS'])-1), holidays=holidays, weekends=default_weekends), row['RADIOTHERAPYDIAGNOSISICD'], row['RTTREATMENTREGION'], row['RTTREATMENTANATOMICALSITE'], float(row['PRESCRIBEDDOSE']), float(row['PRESCRIBEDFRACTIONS']), row['RADIOTHERAPYINTENT'], row['RTTREATMENTMODALITY'], row['GOLDVALUE']))

    return(end_ttt)


# #################################################### #


# ################################# #
# Functions for the defragmentation #
# ################################# #


def defragmentation(list_raw_data):
    list_replan = []

    list_raw_data = sorted(list_raw_data, key=itemgetter(1, 14, 3, 2))

    # Initialisation of the variables
    nhs_number = list_raw_data[0][1]
    dose = 0
    frac = 0
    start_date = list_raw_data[0][4]
    icd = 0
    ttt_region = 0
    anat_site = 0
    cycle = 0

    for row in list_raw_data:
        if row[1] == nhs_number and (row[4] == start_date or cycle == row[14]) and row[6][0:3] == icd and ttt_region in ['P', 'R', 'PR'] and anat_site == row[8] and ((row[6][0:3] in ['C00', 'C01', 'C02', 'C03', 'C04', 'C05', 'C06', 'C07', 'C08', 'C09', 'C10', 'C11', 'C12', 'C13', 'C14', 'C30', 'C31', 'C32'] and row[7] in ['P', 'R', 'PR'] and ((round((float(dose) + float(row[2])), 0) == 50 and round((float(frac) + float(row[3])), 0) == 16) or (round((float(dose) + float(row[2])), 0) == 50 and round((float(frac) + float(row[3])), 0) == 20) or (round((float(dose) + float(row[2])), 0) == 55 and round((float(frac) + float(row[3])), 0) == 20) or (round((float(dose) + float(row[2])), 0) == 55 and round((float(frac) + float(row[3])), 0) == 25) or (round((float(dose) + float(row[2])), 0) == 63 and round((float(frac) + float(row[3])), 0) == 28) or (round((float(dose) + float(row[2])), 0) == 54 and round((float(frac) + float(row[3])), 0) == 30) or (round((float(dose) + float(row[2])), 0) == 60 and round((float(frac) + float(row[3])), 0) == 30) or (round((float(dose) + float(row[2])), 0) in [65, 66] and round((float(frac) + float(row[3])), 0) == 30) or (round((float(dose) + float(row[2])), 0) == 66 and round((float(frac) + float(row[3])), 0) == 33) or (round((float(dose) + float(row[2])), 0) in [56, 57] and round((float(frac) + float(row[3])), 0) == 35) or (round((float(dose) + float(row[2])), 0) == 70 and round((float(frac) + float(row[3])), 0) == 33) or (round((float(dose) + float(row[2])), 0) == 70 and round((float(frac) + float(row[3])), 0) == 35))) or (row[6][0:3] == 'C15' and row[7] in ['P', 'R', 'PR'] and ((round((float(dose) + float(row[2])), 0) == 50 and round((float(frac) + float(row[3])), 0) in [15, 16, 25, 28]) or (round((float(dose) + float(row[2])), 0) in [50, 51, 52, 53, 54, 55] and round((float(frac) + float(row[3])), 0) == 20) or (round((float(dose) + float(row[2])), 0) == 41 and round((float(frac) + float(row[3])), 0) == 23) or (round((float(dose) + float(row[2])), 0) == 60 and round((float(frac) + float(row[3])), 0) == 30) or (round((float(dose) + float(row[2])), 0) == 45 and round((float(frac) + float(row[3])), 0) == 35))) or (row[6][0:3] in ['C18', 'C19', 'C20', 'C21'] and row[7] in ['P', 'R', 'PR'] and (round(float(dose), 1) != 5.4 and round(float(frac), 1) != 3 and round(float(row[2]), 0) != 45 and round(float(row[3]), 0) != 25) and ((round((float(dose) + float(row[2])), 0) == 25 and round((float(frac) + float(row[3])), 0) == 5) or (round((float(dose) + float(row[2])), 0) == 45 and round((float(frac) + float(row[3])), 0) == 25) or (round((float(dose) + float(row[2])), 0) == 50 and round((float(frac) + float(row[3])), 0) == 28) or (round((float(dose) + float(row[2])), 0) == 53 and round((float(frac) + float(row[3])), 0) == 28))) or (row[6][0:3] in ['C33', 'C34'] and row[7] in ['P', 'R', 'PR'] and ((round((float(dose) + float(row[2])), 0) == 10 and round((float(frac) + float(row[3])), 0) == 1) or (round((float(dose) + float(row[2])), 0) == 17 and round((float(frac) + float(row[3])), 0) == 2) or (round((float(dose) + float(row[2])), 0) == 54 and round((float(frac) + float(row[3])), 0) == 3) or (round((float(dose) + float(row[2])), 0) in [20, 55, 60] and round((float(frac) + float(row[3])), 0) == 5) or (round((float(dose) + float(row[2])), 0) == 60 and round((float(frac) + float(row[3])), 0) == 8) or (round((float(dose) + float(row[2])), 0) == 30 and round((float(frac) + float(row[3])), 0) == 10) or (round((float(dose) + float(row[2])), 0) == 36 and round((float(frac) + float(row[3])), 0) == 12) or (round((float(dose) + float(row[2])), 0) == 39 and round((float(frac) + float(row[3])), 0) == 13) or (round((float(dose) + float(row[2])), 0) == 40 and round((float(frac) + float(row[3])), 0) == 15) or (round((float(dose) + float(row[2])), 0) == 55 and round((float(frac) + float(row[3])), 0) == 20) or (round((float(dose) + float(row[2])), 0) in [45, 50] and round((float(frac) + float(row[3])), 0) == 25) or (round((float(dose) + float(row[2])), 0) in [45, 60] and round((float(frac) + float(row[3])), 0) == 30) or (round((float(dose) + float(row[2])), 0) == 66 and round((float(frac) + float(row[3])), 0) == 33) or (round((float(dose) + float(row[2])), 0) == 54 and round((float(frac) + float(row[3])), 0) == 36))) or (row[6][0:3] in ['C43', 'C44'] and row[7] in ['P', 'R', 'PR'] and ((round((float(dose) + float(row[2])), 0) in [18, 19, 20] and round((float(frac) + float(row[3])), 0) == 1) or (round((float(dose) + float(row[2])), 0) in [32, 33, 34, 35] and round((float(frac) + float(row[3])), 0) == 5) or (round((float(dose) + float(row[2])), 0) == 45 and round((float(frac) + float(row[3])), 0) == 10) or (round((float(dose) + float(row[2])), 0) in [48, 55] and round((float(frac) + float(row[3])), 0) == 20) or (round((float(dose) + float(row[2])), 0) == 60 and round((float(frac) + float(row[3])), 0) == 30) or (round((float(dose) + float(row[2])), 0) == 66 and round((float(frac) + float(row[3])), 0) == 33))) or (row[6][0:3] == 'C50' and row[7] in ['P', 'R', 'PR'] and (round(float(dose), 0) != 10 and round(float(row[2]), 0) != 40) and ((round((float(dose) + float(row[2])), 0) in [39, 40, 41] and round((float(frac) + float(row[3])), 0) == 15) or (round((float(dose) + float(row[2])), 0) == 50 and round((float(frac) + float(row[3])), 0) == 25) or (round((float(dose) + float(row[2])), 0) == 10 and round((float(frac) + float(row[3])), 0) == 5) or (round((float(dose) + float(row[2])), 0) == 36 and round((float(frac) + float(row[3])), 0) == 6))) or (row[6][0:3] in ['C54', 'C55'] and row[7] in ['P', 'R', 'PR'] and (round(dose, 0) != 8 and round(frac, 0) != 2) and ((round((float(dose) + float(row[2])), 0) == 28 and round((float(frac) + float(row[3])), 0) == 4) or (round((float(dose) + float(row[2])), 0) in [25, 36] and round((float(frac) + float(row[3])), 0) == 5) or (round((float(dose) + float(row[2])), 0) in [37, 38] and round((float(frac) + float(row[3])), 0) == 6) or (round((float(dose) + float(row[2])), 0) == 40 and round((float(frac) + float(row[3])), 0) == 20) or (round((float(dose) + float(row[2])), 0) == 46 and round((float(frac) + float(row[3])), 0) == 23) or (round((float(dose) + float(row[2])), 0) in [45, 50] and round((float(frac) + float(row[3])), 0) == 25) or (round((float(dose) + float(row[2])), 0) in [48, 49] and round((float(frac) + float(row[3])), 0) == 27) or (round((float(dose) + float(row[2])), 0) == 50 and round((float(frac) + float(row[3])), 0) == 28))) or (row[6][0:3] == 'C61' and row[7] in ['P', 'R', 'PR'] and ((round((float(dose) + float(row[2])), 0) in [52, 53, 60] and round((float(frac) + float(row[3])), 0) == 20) or (round((float(dose) + float(row[2])), 0) in [55, 56, 57, 58, 59, 60] and round((float(frac) + float(row[3])), 0) == 37) or (round((float(dose) + float(row[2])), 0) in [74, 75, 76, 77, 78] and round((float(frac) + float(row[3])), 0) in [37, 38, 39]) or (round((float(dose) + float(row[2])), 0) == 66 and round((float(frac) + float(row[3])), 0) == 33))) or (row[6][0:3] == 'C67' and row[7] in ['P', 'R', 'PR'] and ((round((float(dose) + float(row[2])), 0) in [52, 53, 54, 55] and round((float(frac) + float(row[3])), 0) == 20) or (round((float(dose) + float(row[2])), 0) in [60, 61, 62, 63, 64] and round((float(frac) + float(row[3])), 0) in [30, 31, 32]))) or (row[6][0:3] in ['C70', 'C71', 'C72'] and row[7] in ['P', 'R', 'PR'] and ((round((float(dose) + float(row[2])), 0) == 60 and round((float(frac) + float(row[3])), 0) == 30) or (round((float(dose) + float(row[2])), 0) in [59, 60] and round((float(frac) + float(row[3])), 0) == 33) or (round((float(dose) + float(row[2])), 0) == 40 and round((float(frac) + float(row[3])), 0) == 15) or (round((float(dose) + float(row[2])), 0) == 34 and round((float(frac) + float(row[3])), 0) == 10) or (round((float(dose) + float(row[2])), 0) == 30 and round((float(frac) + float(row[3])), 0) == 6) or (round((float(dose) + float(row[2])), 0) == 50 and round((float(frac) + float(row[3])), 0) == 28) or (round((float(dose) + float(row[2])), 0) == 60 and round((float(frac) + float(row[3])), 0) == 30) or (round((float(dose) + float(row[2])), 0) == 50 and round((float(frac) + float(row[3])), 0) == 28) or (round((float(dose) + float(row[2])), 0) == 54 and round((float(frac) + float(row[3])), 0) == 30) or (round((float(dose) + float(row[2])), 0) in range(50, 54, 1) and round((float(frac) + float(row[3])), 0) in [28, 29, 30]) or (round((float(dose) + float(row[2])), 0) in range(54, 60, 1) and round((float(frac) + float(row[3])), 0) == 30) or (round((float(dose) + float(row[2])), 0) == 45 and round((float(frac) + float(row[3])), 0) == 25) or (round((float(dose) + float(row[2])), 0) in range(50, 55, 1) and round((float(frac) + float(row[3])), 0) in range(30, 33, 1)) or (round((float(dose) + float(row[2])), 0) in range(52, 54, 1) and round((float(frac) + float(row[3])), 0) in [27, 28])))):

            for index, item in enumerate(list_replan):
                if item[0] == row[0] and item[1] == row[1] and float(item[2]) == dose and float(item[3]) == frac and item[4] == start_date:
                    list_t = list(list_replan[index])
                    list_t[2] = round((float(dose) + float(row[2])), 1)  # Change the actual dose received
                    list_t[3] = float(frac) + float(row[3])  # Change the fraction
                    list_t[15] = 'Changed'
                    if item[13] != row[13]:
                        list_t[13] = list_t[13] + " and " + row[13]
                    list_replan[index] = tuple(list_t)

            # Initialisation with the actual line
            nhs_number = row[1]
            dose = float(row[2])
            frac = float(row[3])
            start_date = row[4]
            icd = row[6][0:3]
            ttt_region = row[7]
            anat_site = row[8]
            cycle = row[14]

        else:
            if row[15] == 'Changed':
                list_replan.append((row[0], row[1], float(row[2]), float(row[3]), row[4], row[5], row[6], row[7], row[8], float(row[9]), float(row[10]), row[11], row[12], row[13], row[14], row[15]))
            else:
                list_replan.append((row[0], row[1], float(row[2]), float(row[3]), row[4], row[5], row[6], row[7], row[8], float(row[9]), float(row[10]), row[11], row[12], row[13], row[14], 'Not changed'))

            # Initialisation with the actual line
            nhs_number = row[1]
            dose = float(row[2])
            frac = float(row[3])
            start_date = row[4]
            icd = row[6]
            ttt_region = row[7]
            anat_site = row[8]
            cycle = row[14]

    # Sort the new list by Trust, NHS number, start date, treatment region and anatomical site, fraction and dose.
    list_replan = sorted(list_replan, key=itemgetter(1, 14, 3, 2))

    return(list_replan)


# #################################################### #


# ######################## #
# Functions for the gluing #
# ######################## #


def cycle(list_raw_data):
    # Create a new empty list to have the next new results
    list_cycle = []

    hosp_number = 0
    ttt_region = 0
    ttt_anat_site = 0
    dose = 0
    frac = 0
    start_ttt = list_raw_data[0][4]
    end_ttt = list_raw_data[0][5]

    cycle_nb = 0

    delai = 30

    for row in list_raw_data:
        if hosp_number == row[1]:
            if (ttt_region == row[7] and ttt_anat_site == row[8] and (start_ttt.date() == row[4].date() or abs(row[4] - end_ttt).days < delai)) or (abs(row[4] - end_ttt).days < delai) or start_ttt.date() == row[4].date():
                cycle_nb = cycle_nb
            else:
                cycle_nb += 1

            list_cycle.append((row[0], row[1], float(row[2]), float(row[3]), row[4], row[5], row[6], row[7], row[8], row[9], row[10], row[11], row[12], row[13], cycle_nb))

            hosp_number = row[1]
            dose = row[2]
            frac = row[3]
            start_ttt = row[4]
            end_ttt = row[5]
            ttt_region = row[7]
            ttt_anat_site = row[8]

        else:
            cycle_nb = 1
            list_cycle.append((row[0], row[1], float(row[2]), float(row[3]), row[4], row[5], row[6], row[7], row[8], row[9], row[10], row[11], row[12], row[13], cycle_nb))

            hosp_number = row[1]
            dose = row[2]
            frac = row[3]
            start_ttt = row[4]
            end_ttt = row[5]
            ttt_region = row[7]
            ttt_anat_site = row[8]

    list_cycle = sorted(list_cycle, key=itemgetter(0, 1, 4, 2, 3))

    return(list_cycle)


def BreastBoost(list_raw_data):
    boost = []

    for row in list_raw_data:
        if row[6][0:3] == 'C50' and row[7] in ['P', 'R', 'PR'] and ((round(float(row[2]), 0) in [9, 10, 11] and float(row[3]) == 3) or (round(float(row[2]), 0) in [8, 12] and float(row[3]) == 4) or (round(float(row[2]), 0) in [10, 13, 14, 15] and float(row[3]) == 5) or (round(float(row[2]), 0) == 16 and float(row[3]) == 8) or (round(float(row[2]), 0) == 20 and float(row[3]) == 10)):
            boost.append((row[0], row[1], float(row[2]), float(row[3]), row[4], row[5], row[6], row[7], row[8], row[9], row[10], row[11], row[12], row[13], row[14], "Boost"))
        else:
            boost.append((row[0], row[1], float(row[2]), float(row[3]), row[4], row[5], row[6], row[7], row[8], row[9], row[10], row[11], row[12], row[13], row[14], "Not a boost"))

    return(boost)


def gluing(list_cycle):
    list_gluing = []

    hosp_number = 0
    cycle = 0

    for row in list_cycle:
        if hosp_number == row[1] and cycle == row[14]:
            for i, item in enumerate(list_gluing):
                if item[1] == row[1] and item[3] == cycle:
                    list_t = list(list_gluing[i])

                    # Add all the doses and fractions received by the patient
                    dose_fraction = str(row[2]) + "Gy in " + str(row[3]) + "#"
                    list_t[5] = list_t[5] + " and " + dose_fraction

                    # Add all the region treated
                    if row[7] not in list_t[6]:
                        list_t[6] = list_t[6] + " + " + row[7]

                    # Add all the anatomical region treated
                    if row[8] not in list_t[6]:
                        list_t[6] = list_t[6] + " + " + row[8]

                    # Add the Gold standard value
                    if row[13].lower() != list_t[7].lower():
                        list_t[7] = list_t[7] + " and " + row[13]

                    if row[15] not in list_t[8]:
                        list_t[8] = list_t[8] + " and " + row[15]

                    list_gluing[i] = tuple(list_t)

            hosp_number = row[1]
            cycle = row[14]
        else:
            dose_fraction = str(row[2]) + "Gy in " + str(row[3]) + "#"

            if row[8] != "":
                ttt_anato = row[7] + " + " + row[8]
            else:
                ttt_anato = row[7]

            list_gluing.append((row[0], row[1], row[6], row[14], row[4], dose_fraction, ttt_anato, row[13], row[15]))

            hosp_number = row[1]
            cycle = row[14]

    return(list_gluing)
