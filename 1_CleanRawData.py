# #################################################### #
# File to clean the raw data extracted from PHE/CAS:
# - replace the null values of Actual Dose by the Prescribed Dose
# - replace the actual intent into something comprehensive
# Creation date: 11/2019
# By: Kerlann Le Calvez
# #################################################### #

# Import libraries and modules
from Functions import *

# #################################################### #

# Paths and files to change
path_input = '.\\Input'
file_input = '1_patients_CAS_raw.csv'
file_output = '2_patients_CAS_raw_cleaned_test.csv'

# Import the csv file into a list
result_csv = import_csv_file(path=path_input, file=file_input)

# #################################################### #

# 0 = ORGCODEPROVIDER
# 1 = NHSNO
# 2 = RTACTUALDOSE
# 3 = ACTUALFRACTIONS
# 4 = TREATMENTSTARTDATE
# 5 = RADIOTHERAPYDIAGNOSISICD
# 6 = RTTREATMENTREGION
# 7 = RTTREATMENTANATOMICALSITE
# 8 = PRESCRIBEDDOSE
# 9 = PRESCRIBEDFRACTIONS
# 10 = RADIOTHERAPYINTENT
# 11 = RTTREATMENTMODALITY

# #################################################### #

# The actual dose received must be positive and not null. If it is, then replace by the prescribed dose and then replace the actual intent by something comprehensive (1 = Palliative; 2 = Anti-Cancer (Not Palliative))
result = clean_intent(clean_doses(result_csv))

# #################################################### #

# Sorted by the radiotherapy centre (ORGCODEPROVIDER), NHS number (NHSNO), diagnosis (RADIOTHERAPYDIAGNOSISICD), area treated (RTTREATMENTREGION & RTTREATMENTANATOMICALSITE), number of fractions (ACTUALFRACTIONS) and actual dose (RTACTUALDOSE).
result = sorted(result, key=itemgetter(0, 1, 5, 6, 7, 3, 2))

# #################################################### #

# Create a list with the headers to export the data with comprehensive items.
result_f = [('ORGCODEPROVIDER', 'NHSNO', 'RTACTUALDOSE', 'ACTUALFRACTIONS', 'TREATMENTSTARTDATE', 'RADIOTHERAPYDIAGNOSISICD', 'RTTREATMENTREGION', 'RTTREATMENTANATOMICALSITE', 'PRESCRIBEDDOSE', 'PRESCRIBEDFRACTIONS', 'RADIOTHERAPYINTENT', 'RTTREATMENTMODALITY')]

# Copy the last list into a final list of results to get the headers
for row in result:
    result_f.append((row[0], row[1], round(float(row[2]), 1), float(row[3]), row[4], row[5], row[6], row[7], float(row[8]), float(row[9]), row[10], row[11]))

# #################################################### #

# Export the final list into a csv file
export_into_csv(path=path_input, file=file_output, final_list=result_f)
