# #################################################### #
# File to glue the radiotherapy episodes together
# Creation date: 11/2019
# By: Kerlann Le Calvez
# #################################################### #

# Import libraries and modules
from Functions import *

# #################################################### #

# Path and file names to change
path_input = '.\\Output\\2_AfterDefragmentation\\4_Old'
path_output = '.\\Output\\2_AfterDefragmentation'

file_input_1 = 'result_test_old_1.csv'
file_input_2 = 'result_test_old_2.csv'
file_input_3 = 'result_test_old_3.csv'
file_input_4 = 'result_test_old_4.csv'
file_input_5 = 'result_test_old_5.csv'

file_output = 'classification.csv'

# #################################################### #

# Change the path of where the files are
os.chdir(path_input)

# Import the files into dataframes
df1 = pd.read_csv(file_input_1)
df2 = pd.read_csv(file_input_2)
df3 = pd.read_csv(file_input_3)
df4 = pd.read_csv(file_input_4)
df5 = pd.read_csv(file_input_5)

# List of all the dataframes
frames = [df1, df2, df3, df4, df5]

# Concatenate the five dataframes into a single one
result = pd.concat(frames)

# #################################################### #

# Change the path to the source
os.chdir(path_output)

# Export the dataframe into a csv file
result.to_csv(file_output, index=False)
