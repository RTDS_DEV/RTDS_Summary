# #################################################### #
# File to find the patients with fragmented radiotherapy episodes
# Creation date: 11/2019
# By: Kerlann Le Calvez
# #################################################### #

# Import libraries and modules
from Functions import *

# #################################################### #

# Path and file names to change
path_input = '.\\Input'
file_input = "3_patients_raw_withGV.csv"
file_output = '4_patients_defragment_withGV.csv'

# Import the csv file into a list
result = import_csv_file(path=path_input, file=file_input)

# #################################################### #

# 0 = ORGCODEPROVIDER
# 1 = NHSNO
# 2 = RTACTUALDOSE
# 3 = ACTUALFRACTIONS
# 4 = TREATMENTSTARTDATE
# 5 = RADIOTHERAPYDIAGNOSISICD
# 6 = RTTREATMENTREGION
# 7 = RTTREATMENTANATOMICALSITE
# 8 = PRESCRIBEDDOSE
# 9 = PRESCRIBEDFRACTIONS
# 10 = RADIOTHERAPYINTENT
# 11 = RTTREATMENTMODALITY
# 12 = GOLDVALUE

# #################################################### #

# Apply the cycle function onto the cohort
list_cycle = cycle(AddEndDate(result))

# Must recreate a new list to add an empty item to know if the course has been changed or not
replan_0 = []

for row in list_cycle:
	replan_0.append((row[0], row[1], float(row[2]), float(row[3]), row[4], row[5], row[6], row[7], row[8], float(row[9]), float(row[10]), row[11], row[12], row[13], row[14], ''))

# Apply the defragmentation function twice
replan = defragmentation(defragmentation(replan_0))

# #################################################### #

# Create a list with the headers to export the data with comprehensive items.
replan_f = [('ORGCODEPROVIDER', 'NHSNO', 'RTACTUALDOSE', 'ACTUALFRACTIONS', 'TREATMENTSTARTDATE', 'TREATMENTENDDATE', 'RADIOTHERAPYDIAGNOSISICD', 'RTTREATMENTREGION', 'RTTREATMENTANATOMICALSITE', 'PRESCRIBEDDOSE', 'PRESCRIBEDFRACTIONS', 'RADIOTHERAPYINTENT', 'RTTREATMENTMODALITY', 'GOLDVALUE', 'Cycle', 'Changed?')]

# Copy the last list into a final list of results to get the headers
for row in replan:
    replan_f.append((row[0], row[1], float(row[2]), float(row[3]), row[4], row[5], row[6], row[7], row[8], float(row[9]), float(row[10]), row[11], row[12], row[13], row[14], row[15]))

# #################################################### #

# Export the final list into a csv file
export_into_csv(path=path_input, file=file_output, final_list=replan_f)
