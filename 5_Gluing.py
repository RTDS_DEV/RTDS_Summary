# #################################################### #
# File to glue the radiotherapy episodes together
# Creation date: 11/2019
# By: Kerlann Le Calvez
# #################################################### #

# Import libraries and modules
from Functions import *

# #################################################### #

# Path and file names to change
path_input = '.\\Input'
file_input = "4_patients_defragment_withGV.csv"
file_output_boost = '5_patients_boost_withGV.csv'
file_output_gluing = '6_patients_gluing_withGV.csv'

# Import the csv file into a list
result = clean_types(import_csv_file(path=path_input, file=file_input))

# #################################################### #

# 0 = ORGCODEPROVIDER
# 1 = NHSNO
# 2 = RTACTUALDOSE
# 3 = ACTUALFRACTIONS
# 4 = TREATMENTSTARTDATE
# 5 = TREATMENTENDDATE
# 6 = RADIOTHERAPYDIAGNOSISICD
# 7 = RTTREATMENTREGION
# 8 = RTTREATMENTANATOMICALSITE
# 9 = PRESCRIBEDDOSE
# 10 = PRESCRIBEDFRACTIONS
# 11 = RADIOTHERAPYINTENT
# 12 = RTTREATMENTMODALITY
# 13 = GOLDVALUE

# #################################################### #

# Apply the cycle function to know how many courses of radiotherapy the patients had and then apply the gluing function to get a summary.
boost = BreastBoost(cycle(result))
gluing_ttt = gluing(boost)

# #################################################### #

# For the boost
# Create a list with the headers to export the data with comprehensive items.
boost_f = [('ORGCODEPROVIDER', 'NHSNO', 'RTACTUALDOSE', 'ACTUALFRACTIONS', 'TREATMENTSTARTDATE', 'TREATMENTENDDATE', 'RADIOTHERAPYDIAGNOSISICD', 'RTTREATMENTREGION', 'RTTREATMENTANATOMICALSITE', 'PRESCRIBEDDOSE', 'PRESCRIBEDFRACTIONS', 'RADIOTHERAPYINTENT', 'RTTREATMENTMODALITY', 'GOLDVALUE', 'Cycle', 'BreastBoost')]

# Copy the last list into a final list of results to get the headers
for row in boost:
	boost_f.append((row[0], row[1], float(row[2]), float(row[3]), row[4], row[5], row[6], row[7], row[8], float(row[9]), float(row[10]), row[11], row[12], row[13], row[14], row[15]))

# For the gluing
# Create a list with the headers to export the data with comprehensive items.
gluing_ttt_f = [('ORGCODEPROVIDER', 'NHSNO', 'RADIOTHERAPYDIAGNOSISICD', 'NUMBERTREATMENT', 'TREATMENTSTARTDATE', 'DOSEFRACTIONATION', 'TREATMENTREGIONANATOMICALSITE', 'GOLDVALUE', 'BREASTBOOST')]

# Copy the last list into a final list of results to get the headers
for row in gluing_ttt:
    gluing_ttt_f.append((row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8]))

# #################################################### #

# Export the final list into a csv file
export_into_csv(path=path_input, file=file_output_boost, final_list=boost_f)
export_into_csv(path=path_input, file=file_output_gluing, final_list=gluing_ttt_f)
