# ######################################### #
# Performance for the rule-based classifier #
# ######################################### #

# ################ #
# Import libraries #
# ################ #
import os
# To handle the .csv files
import pandas as pd
# To measure the performance of the model
from sklearn.metrics import confusion_matrix
import numpy as np

# ######### #
# Functions #
# ######### #


# Measure the performance of the classifier using the confusion matrix
def PerfMeasure(y_true, y_pred):
    cm = confusion_matrix(y_true, y_pred, labels=['Palliative', 'Not Palliative'])
    TP = cm[0, 0]
    FN = cm[0, 1]
    FP = cm[1, 0]
    TN = cm[1, 1]

    accuracy = round(float((TP+TN)/(TP+TN+FP+FN)), 3)
    sensitivity = round(float(TP/(TP+FN)), 3)
    specificity = round(float(TN/(TN+FP)), 3)
    precision = round(float(TP/(TP+FP)), 3)
    f1score = round(float((2*sensitivity*precision)/(sensitivity+precision)), 3)

    return("accuracy : ", accuracy, "sensitivity : ", sensitivity, "specificity : ", specificity, "precision : ", precision, "F1-score : ", f1score)

# ##############################

# Performance for the rule-based

# ############# #
# Load the data #
# ############# #

# path_output = '.\\Output\\1_Raw\\4_Old'
path_output = '.\\Output\\2_AfterDefragmentation\\4_Old'

os.chdir(path_output)

palliative = pd.read_csv("result_test_old_5.csv")

# ######################## #
# Performance of the model #
# ######################## #

# Print the performance for the SVM algorithm
print("Value predicted SVM")
print(PerfMeasure(palliative['GOLDVALUE'], palliative['ValuePredicted_SVM']))
print("\n")

# Print the performance for the rule-based algorithm
print("Value predicted Rule-based")
print(PerfMeasure(palliative['GOLDVALUE'], palliative['ValuePredicted_RuleBased']))
print("\n")

# Print the performance for the old algorithm
print("Value predicted Old")
print(PerfMeasure(palliative['GOLDVALUE'], palliative['ValuePredicted_Old']))
print("\n")

# Remove the empty rows and the rows for which there is "Missing data" in the RadiotherapyIntent
palliative = palliative[pd.notnull(palliative['RADIOTHERAPYINTENT'])]
palliative = palliative[palliative.RADIOTHERAPYINTENT != 'Missing data']

# Print the performance for the direct method
print("Radiotherapy Intent")
print(PerfMeasure(palliative['GOLDVALUE'], palliative['RADIOTHERAPYINTENT']))
