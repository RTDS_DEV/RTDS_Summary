# #################################################### #
# File to find the patients with palliative radiotherapy with the rule-based algorithm
# Creation date: 11/2019
# By: Kerlann Le Calvez
# #################################################### #

# Import libraries and modules
from Functions import *

# #################################################### #

# Paths to change

# First step on raw data
# path_input = ".\\Output\\1_Raw\\1_SVM"
# path_output = ".\\Output\\1_Raw\\2_RB"

# Second step on defragmented data
path_input = ".\\Output\\2_AfterDefragmentation\\1_SVM"
path_output = ".\\Output\\2_AfterDefragmentation\\2_RB"

# Files to update
file_input = "result_test_svm_5.csv"
file_output = 'result_test_rb_5.csv'

# #################################################### #

# Import the csv file into a list
result = import_csv_file(path=path_input, file=file_input)

# #################################################### #

# 0 = ORGCODEPROVIDER
# 1 = NHSNO
# 2 = RTACTUALDOSE
# 3 = ACTUALFRACTIONS
# 4 = TREATMENTSTARTDATE
# 5 = RADIOTHERAPYDIAGNOSISICD
# 6 = RTTREATMENTREGION
# 7 = RTTREATMENTANATOMICALSITE
# 8 = PRESCRIBEDDOSE
# 9 = PRESCRIBEDFRACTIONS
# 10 = RADIOTHERAPYINTENT
# 11 = RTTREATMENTMODALITY
# 12 = GOLDVALUE
# 13 = ValuePredicted_SVM

# #################################################### #

# Run the rule-based algorithm
classification_rb = RuleBased(result)

# #################################################### #

# Create a list with the headers to export the data with comprehensive items.
classification_rb_f = [('ORGCODEPROVIDER', 'NHSNO', 'RTACTUALDOSE', 'ACTUALFRACTIONS', 'TREATMENTSTARTDATE', 'RADIOTHERAPYDIAGNOSISICD', 'RTTREATMENTREGION', 'RTTREATMENTANATOMICALSITE', 'PRESCRIBEDDOSE', 'PRESCRIBEDFRACTIONS', 'RADIOTHERAPYINTENT', 'RTTREATMENTMODALITY', 'GOLDVALUE', 'ValuePredicted_SVM', 'ValuePredicted_RuleBased')]

# Copy the last list into a final list of results to get the headers
for row in classification_rb:
	classification_rb_f.append((row[0], row[1], float(row[2]), float(row[3]), row[4], row[5], row[6], row[7], float(row[8]), float(row[9]), row[10], row[11], row[12], row[13], row[14]))

# #################################################### #

# Export the final list into a csv file
export_into_csv(path=path_output, file=file_output, final_list=classification_rb_f)
