# ################################# #
# Support Vector Machine classifier #
# ################################# #

# ################ #
# Import libraries #
# ################ #
from Functions import *

# ####################### #
# Load and clean the data #
# ####################### #

# Change the path to work in the correct folder
# The input folder won't change, only the file will change
path_input = ".\\Input"

# The output folder will change, depending on the progress of the project
# path_output = ".\\Output\\1_Raw\\1_SVM"
path_output = ".\\Output\\2_AfterDefragmentation\\1_SVM"

os.chdir(path_input)

# From plain csv file
file_input = "4_patients_defragment_withGV.csv"
palliative = clean_csv(pd.read_csv(file_input))

# Keep the variables for the SVM
palliative_SVM = palliative[['RTACTUALDOSE', 'ACTUALFRACTIONS', 'RADIOTHERAPYDIAGNOSISICD', 'RTTREATMENTREGION', 'RTTREATMENTANATOMICALSITE', 'PRESCRIBEDDOSE', 'PRESCRIBEDFRACTIONS', 'RTTREATMENTMODALITY', 'GOLDVALUE']]
palliative_ids = palliative[['ORGCODEPROVIDER', 'NHSNO', 'TREATMENTSTARTDATE', 'RADIOTHERAPYINTENT']]
del palliative

# ####################################################### #
# We want to keep the true values and their preprocessing #
# ####################################################### #

# Copy the original columns into new columns
palliative_SVM['RTActualDose_keep'] = palliative_SVM['RTACTUALDOSE']
palliative_SVM['ActualFractions_keep'] = palliative_SVM['ACTUALFRACTIONS']
palliative_SVM['RadiotherapyDiagnosisICD_keep'] = palliative_SVM['RADIOTHERAPYDIAGNOSISICD']
palliative_SVM['RTTreatmentRegion_keep'] = palliative_SVM['RTTREATMENTREGION']
palliative_SVM['RTTreatmentAnatomicalSite_keep'] = palliative_SVM['RTTREATMENTANATOMICALSITE']
palliative_SVM['PrescribedDose_keep'] = palliative_SVM['PRESCRIBEDDOSE']
palliative_SVM['PrescribedFractions_keep'] = palliative_SVM['PRESCRIBEDFRACTIONS']
palliative_SVM['RTTreatmentModality_keep'] = palliative_SVM['RTTREATMENTMODALITY']
palliative_SVM['GoldValue_keep'] = palliative_SVM['GOLDVALUE']

# Label the datasets for the training and test (Preprocessing)
WholeLabelString(palliative_SVM)

# Copy the dataframe into a new one
RTActualDose = palliative_SVM.copy()
ActualFractions = palliative_SVM.copy()
RadiotherapyDiagnosisICD = palliative_SVM.copy()
RTTreatmentRegion = palliative_SVM.copy()
RTTreatmentAnatomicalSite = palliative_SVM.copy()
PrescribedDose = palliative_SVM.copy()
PrescribedFractions = palliative_SVM.copy()
RadiotherapyIntent = palliative_SVM.copy()
RTTreatmentModality = palliative_SVM.copy()
GoldValue = palliative_SVM.copy()

# Remove all the colums that are not used so we can rebuild the dataset at the end
RTActualDose = RTActualDose[["RTActualDose_keep", "RTACTUALDOSE"]]
RTActualDose = RTActualDose.drop_duplicates()

ActualFractions = ActualFractions[["ActualFractions_keep", "ACTUALFRACTIONS"]]
ActualFractions = ActualFractions.drop_duplicates()

RadiotherapyDiagnosisICD = RadiotherapyDiagnosisICD[["RadiotherapyDiagnosisICD_keep", "RADIOTHERAPYDIAGNOSISICD"]]
RadiotherapyDiagnosisICD = RadiotherapyDiagnosisICD.drop_duplicates()

RTTreatmentRegion = RTTreatmentRegion[["RTTreatmentRegion_keep", "RTTREATMENTREGION"]]
RTTreatmentRegion = RTTreatmentRegion.drop_duplicates()

RTTreatmentAnatomicalSite = RTTreatmentAnatomicalSite[["RTTreatmentAnatomicalSite_keep", "RTTREATMENTANATOMICALSITE"]]
RTTreatmentAnatomicalSite = RTTreatmentAnatomicalSite.drop_duplicates()

PrescribedDose = PrescribedDose[["PrescribedDose_keep", "PRESCRIBEDDOSE"]]
PrescribedDose = PrescribedDose.drop_duplicates()

PrescribedFractions = PrescribedFractions[["PrescribedFractions_keep", "PRESCRIBEDFRACTIONS"]]
PrescribedFractions = PrescribedFractions.drop_duplicates()

RTTreatmentModality = RTTreatmentModality[["RTTreatmentModality_keep", "RTTREATMENTMODALITY"]]
RTTreatmentModality = RTTreatmentModality.drop_duplicates()

GoldValue = GoldValue[["GoldValue_keep", "GOLDVALUE"]]
GoldValue = GoldValue.drop_duplicates()

# Remove the column "PatientId_keep" (not used for the model)
del palliative_SVM['RTActualDose_keep']
del palliative_SVM['ActualFractions_keep']
del palliative_SVM['RadiotherapyDiagnosisICD_keep']
del palliative_SVM['RTTreatmentRegion_keep']
del palliative_SVM['RTTreatmentAnatomicalSite_keep']
del palliative_SVM['PrescribedDose_keep']
del palliative_SVM['PrescribedFractions_keep']
del palliative_SVM['RTTreatmentModality_keep']
del palliative_SVM['GoldValue_keep']

# Rename the columns "XX" into "XX_keep" to match the names of the other dataframes
palliative_SVM = palliative_SVM.rename(columns={'RTACTUALDOSE': 'RTActualDose_keep', 'ACTUALFRACTIONS': 'ActualFractions_keep',  'RADIOTHERAPYDIAGNOSISICD': 'RadiotherpyDiagnosisICD_keep', 'RTTREATMENTREGION': 'RTTreatmentRegion_keep', 'RTTREATMENTANATOMICALSITE': 'RTTreatmentAnatomicalSite_keep', 'PRESCRIBEDDOSE': 'PrescribedDose_keep', 'PRESCRIBEDFRACTIONS': 'PrescribedFractions_keep', 'RTTREATMENTMODALITY': 'RTTreatmentModality_keep', 'GOLDVALUE': 'GoldValue_keep'})

# ###################### #
# Building the SVM model #
# ###################### #

# Change the value into a table
palliative_SVM = palliative_SVM.reset_index()
palliative_ids = palliative_ids.rename_axis('Indices').reset_index()
array = palliative_SVM.values

# 5-fold cross validation
kf = model_selection.KFold(n_splits=5, shuffle=True)
kf.get_n_splits(array)

# Select the predictor (X) and the target (Y) but must keep the index to be able to rebuild the dataset later
X = array[:, 0:9]
Y = array[:, [0, 9]]

count = 0

for train_index, test_index in kf.split(X):
    count = int(count) + 1
    X_train, X_test = X[train_index], X[test_index]
    Y_train, Y_test = Y[train_index], Y[test_index]

    # Model
    model = svm.SVC(kernel='linear')
    model.fit(X_train[:, 1:8], Y_train[:, 1])  # For X_train: select only the predictors and exclude the index, same for Y_train: index excluded and the target is kept

    # Predict the test dataset
    predicted = model.predict(X_test[:, 1:8])

    # ######################## #
    # Performance of the model #
    # ######################## #

    # # Confusion matrix to get the performance
    # matrix = confusion_matrix(Y_test[:,1], predicted, labels = [1, 0])
    # print(matrix)

    # Print the sensitivity and specificity
    print("Round ", count, " : ", PerfMeasure(Y_test[:,1], predicted), "\n")

    ## ############################ #
    ## Clean the data and export it #
    ## ############################ #
    result_train = gather(X=X_train, Y=Y_train, DatasetIDs=palliative_ids, A_Dose=RTActualDose, A_Fracs=ActualFractions, ICD=RadiotherapyDiagnosisICD, Region=RTTreatmentRegion, Site=RTTreatmentAnatomicalSite, P_Dose=PrescribedDose, P_Fracs=PrescribedFractions, Modality=RTTreatmentModality, Gold=GoldValue, dataset='train')
    result_test = gather(X=X_test, Y=Y_test, DatasetIDs=palliative_ids, Prediction=predicted, A_Dose=RTActualDose, A_Fracs=ActualFractions, ICD=RadiotherapyDiagnosisICD, Region=RTTreatmentRegion, Site=RTTreatmentAnatomicalSite, P_Dose=PrescribedDose, P_Fracs=PrescribedFractions, Modality=RTTreatmentModality, Gold=GoldValue, dataset='test')

    # Create file names
    name_train = str("result_train_svm_" + str(count) + ".csv")
    name_test = str("result_test_svm_" + str(count) + ".csv")

    # Export the files to the drive
    os.chdir(path_output)
    result_train.to_csv(name_train, index=False)
    result_test.to_csv(name_test, index=False)
